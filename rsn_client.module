<?php

/**
 * @file
 * RSN Client module hooks.
 */

/**
 * Implements hook_menu().
 */
function rsn_client_menu() {

  $items['admin/config/services/rsn'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'RSN',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rsn_client_admin_config'),
    'access arguments' => array(''),
    'file' => 'rsn_client.admin.inc',
  );

  return $items;
}

/**
 * Executes a request to the RSN Service.
 */
function rsn_client_query_service($method, $params = array(), $endpoint = NULL) {

  // Fallback to configuration if endpoint not specified.
  if (!$endpoint) {
    $endpoint = variable_get('rsn_client_endpoint');
  }

  // Bail if endpoint not available.
  if (!$endpoint) {
    return;
  }

  // Allow the url to be altered by other modules.
  drupal_alter('rsn_client_endpoint', $endpoint);

  // Craft a URL out of the endpoint and method.
  $url = trim($endpoint);
  if (substr($url, -1) != '/') {
    $url .= '/';
  }
  $url .= trim($method);

  // Add in the key.
  if (empty($params['key'])) {
    $params['key'] = variable_get('rsn_client_key');
  }
  $params = drupal_http_build_query($params);
  $url .= '?' . $params;

  return drupal_http_request($url);
}

/**
 * Get newsletters available from RSN Service.
 */
function rsn_client_fetch_newsletters() {

  // If we've got cached data, use it instead of doing HTTP request.
  $cache = cache_get(__FUNCTION__);
  if ($cache) {
    return $cache->data;
  }

  $data = rsn_client_query_service('newsletter');
  if ($data->code == 200) {
    $json = json_decode($data->data);
    $newsletters = array();
    foreach ($json->data as $newsletter) {
      $newsletters[$newsletter->nid] = $newsletter->name;
    }

    cache_set(__FUNCTION__, $newsletters);
    return $newsletters;
  }
}
