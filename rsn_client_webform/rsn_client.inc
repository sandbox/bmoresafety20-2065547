<?php

/**
 * @file
 * RSN Client Webform Component.
 */

/**
 * Implements _webform_defaults_rsn_client().
 *
 * Default webform component configuration for rsn_client.
 */
function _webform_defaults_rsn_client() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'extra' => array(
      'mail' => FALSE,
      'prompt' => '',
      'default' => TRUE,
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_edit_component($component).
 *
 * Generate the form for editing a component.
 */
function _webform_edit_rsn_client($component) {

  $newsletters = rsn_client_fetch_newsletters();
  $form['value'] = array(
    '#type' => 'select',
    '#title' => t('Newsletter'),
    '#default_value' => $component['value'],
    '#description' => t('Select the newsletter that the submission will subscribe to.'),
    '#options' => $newsletters,
  );

  $form['extra']['default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Checked by default'),
    '#default_value' => $component['extra']['default'],
    '#description' => t('Indicate if the subscription component should be checked by default.'),
  );

  $form['extra']['prompt'] = array(
    '#type' => 'textfield',
    '#title' => t('Prompt'),
    '#default_value' => $component['extra']['prompt'],
    '#description' => t('Enter the text to be displayed on the subscribe component. If blank, the default subscription prompt will be used.'),
  );

  $node = node_load($component['nid']);
  $emails = webform_component_list($node, 'email_address', FALSE);
  $form['extra']['mail'] = array(
    '#type' => 'select',
    '#title' => t('Mail component'),
    '#default_value' => $component['extra']['mail'],
    '#description' => t('Select the component that is to be used as the email address for subscribing.'),
    '#options' => $emails,
  );

  return $form;
}

/**
 * Implements _webform_render_component().
 *
 * Renders the rsn_client webform component into the webform.
 */
function _webform_render_rsn_client($component, $value = NULL, $filter = TRUE) {
  $prompt = (!empty($component['extra']['prompt'])) ? $component['extra']['prompt'] : t('Subscribe to :newsletter');
  $element = array(
    '#type' => 'checkbox',
    '#title' => $prompt,
    '#default_value' => $component['extra']['default'],
    '#weight' => $component['weight'],
    '#theme_wrappers' => array('webform_element'),
  );
  return $element;
}
