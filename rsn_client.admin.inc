<?php

/**
 * @file
 * RSN Client module hooks.
 */

/**
 * Form constructor for RSN Client configuration.
 */
function rsn_client_admin_config() {

  $form = array();

  $form['rsn_client_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint'),
    '#default_value' => variable_get('rsn_client_endpoint'),
  );

  $form['rsn_client_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => variable_get('rsn_client_key'),
  );

  return system_settings_form($form);
}
